%% PREPARE_DEMO_DATA
% This script generates demo data that can be used to test the volume
% rendering app. Make sure that the paths defined in gray_scale_output_path
% and label_output_path are as you like and then run the script.

%% Create grayscale and labeled volume demo data
gray_scale_volume = 255*rand(100, 100, 100);
labeled_volume = zeros(size(gray_scale_volume));
labeled_volume(gray_scale_volume > 50) = 100;
labeled_volume(gray_scale_volume > 150) = 200;
labeled_volume(gray_scale_volume > 200) = 255;
labeled_volume = uint8(labeled_volume);
%% Show the results
figure;imshow3Dfull(gray_scale_volume)
figure;imshow3Dfull(label2rgb3d(labeled_volume))

%% Check the number of unique labels
unique(labeled_volume)

%% Write the slices to disk
gray_scale_output_path = 'Gray_scale_volume_demo/demo_sample';
label_output_path = 'Labeled_volume_demo/demo_sample';
mkdir(gray_scale_output_path)
mkdir(label_output_path)

for z = 1:size(gray_scale_volume,3)
    slice = gray_scale_volume(:,:,z);
    file_name = fullfile(gray_scale_output_path,...
                         strcat('slice',sprintf('%03i', z), '.png'));
    imwrite(slice, file_name);
    
    slice = labeled_volume(:,:,z);
    file_name = fullfile(label_output_path,...
                         strcat('slice',sprintf('%03i', z), '.png'));
    imwrite(slice, file_name);
end

%% Test if labeled volume has the same unique values af before saving
clc
label_test = ImportSlices(label_output_path);
unique(label_test)