# Volume Rendering App #
Volume rendering app in MATLAB to visualize gray scale and labeled volume data.

<img src="https://gitlab.com/tim.vandelooverbosch/volume_rendering_app/raw/master/images/interface_02.jpg" width=30%>

Checkout some more screenshots [here](https://gitlab.com/tim.vandelooverbosch/volume_rendering_app/tree/master/images).

## Features ##
- Simultaneous volume rendering of both gray scale and labeled volume data
- Labeled data up to 4 different labeld, in which label "0" is the background and not rendered by default
- Visibility toggles for the different labels
- Opacity sliders and value fields for the different labels
- Color selection option for each label
- Volume "cropping" for both gray scale and labeled volume
- Various default preferences that can be set by the user (label color, opacity, background color...)

## Requirements ##
- To install the app in your matlab installation, you'll need Matlab R2019a.
- To use this app as a standalone application, simple install the binary version of the app

## How to use ##
The fastest way to test and to use the app, is by first cloning or downloading this repository and then run the included "prepare_demo_data.m" script in Matlab. The data can also be downloaded from [here](https://kuleuven.box.com/s/9e6qjm39ji6igrp87n8c2r6qxj1eo57v).
This script will generate some random data for you to play around with, including a gray scale volume (located in "./Gray_scale_volume_demo" folder) and a labeled volume (located in "./Labeled_volume_demo" folder).

Then follow these steps:
- [Install the matlab app](https://nl.mathworks.com/discovery/matlab-apps.html) and launch it.
- Render the gray scale volume, by clicking the "Import gray scale volume data" button and select the "./Gray_scale_volume_demo/demo_sample folder".
- Render the labeled volume, by clicking the "Import labeled data" button and select the "./Gray_scale_volume_demo/demo_sample" folder.
- Change the visibility, opacity or colors of the labels as you like
- Change the start and ending boundaries of the rendered volume, by changing the X, Y and Z slice ranges. Click the refresh button bellow to update the rendered figures.

To open a new volume:
- Click the clear button and redo the steps explained above. Old figures that are still opened will be left open, but can not be updated anymore because the old volumes are removed from memory.

## To use with your own data ##
- The gray scale and labeled volume should have the same dimensions.
- The folders that include the sliced of the gray scale and labeled volume should have the same name. (e.g. grayscale volume in "./raw_data/sample_01" and labeled volume in "./labels/sample_01")

## Setting default user preferences ##
To set some default values for the application, the user can specify various preferences. Checkout the "set_user_preferences.m" script to see how to do this.

## To do ##
Allow user to set default names for label 1, 2 and 3.
