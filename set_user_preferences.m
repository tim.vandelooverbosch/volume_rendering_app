%% SET_USER_PREFERENCES
% Alter and runun this script to set your default preferences

%% Use user preferences
setpref('volume_renderer', 'user_preferences', 1)
%% Label colors
setpref('volume_renderer','label_0_color', [0.0, 0.0, 0.0]);
setpref('volume_renderer','label_1_color', [0.7, 0.7, 0.0]);
setpref('volume_renderer','label_2_color', [0.0, 0.0, 0.7]);
setpref('volume_renderer','label_3_color', [0.0, 0.7, 0.0]);

%% Label opacity
setpref('volume_renderer','label_0_opacity', 0.00);
setpref('volume_renderer','label_1_opacity', 0.05);
setpref('volume_renderer','label_2_opacity', 0.03);
setpref('volume_renderer','label_3_opacity', 0.05);

%% Background colors
setpref('volume_renderer','label_fig_background', [0.8, 0.8, 0.8]);
setpref('volume_renderer','gray_scale_fig_background', [0.8, 0.8, 0.8]);